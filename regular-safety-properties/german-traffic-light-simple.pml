/* Replace with do-loop. 
 *
 * Warning: without "atomic", the light gets out of step with the never claim
 * and fails. The atomic makes it equivalent to the LTS version.
 */
mtype = {red, yellow, red_yellow, green} 
mtype light = yellow

active proctype gtl() {  
r0:
  do
  :: atomic {light == yellow      -> light = red}
	:: atomic {light == red         -> light = red_yellow}
  :: atomic {light == red_yellow  -> light = green}
  :: atomic {light == green       -> light = yellow}
  :: atomic {light == green       -> light = red}
  od
  /*
light = red
r1:
light = red_yellow
r2:
light = green
r3:
if
:: light = yellow
:: light = red
fi
goto r0
*/
}

/* Reaching the end of the never claim indicates an accepting state */
never { 
q0:
  do
  :: light != red && light != yellow
  :: light != red && light == yellow -> break 
  :: light == red -> goto q2
  od
q1:
  do
  :: light != yellow -> goto q0
  :: light == yellow 
  od
q2:
}