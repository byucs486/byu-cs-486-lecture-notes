# Objectives

  * Test to see if an NFA has on empty language
  * Compute the synchronous product of two NFAs
  * Convert an NFA to a DFA
  * Complement a DFA
  * Convert a *labeled transition system* into a DFA or NFA
  * Express regular safety properties as NFA
  * Verify regular safety properties, **on-the-fly** by hand
  * Verify regular safety properties in SPIN
  * Read and understand mathematical notation

# Reading

Sections 4.1 and 4.2 of [Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf)

# DFA and NFA basics

Review briefly

  * NFA structure
  * The meaning of a *run*
  * Acceptance condition
  * Testing if the language is empty (is an accept state reachable --- compute with depth-first-search)
  * Synchronous product
  * Converting an NFA to a DFA
  * Complementing a DFA

Be sure to introduce the set `AP` and explain the meaning of `2^AP` as the power-set of `AP`. These are the letters for the DFA/NFA. As such, `\sigma = 2^AP`.

## Synchronous Product

Put definition 4.8 on page 154 on the board and teach the synchronous product of two NFA. Be sure everyone understands what it says. Work several examples. See paper notes for several examples to work (tab 5).

## NFA to DFA and Complement

**Do not cover** 

Power-set construction. Convert Figure 4.1 on page 151. Complementing a DFA is easy: complement the accept states. Every NFA has a complement because every NFA can be turned into an DFA (though with a potential exponential blowup).

# Verifying Regular Safety Properties

Safety properties always have finite counterexamples. These finite counterexamples are termed *bad prefixes*. Define the set of atomic propositions, `AP`, and then define the letters to describe the bad prefixes with an NFA/DFA as `\Sigma = 2^AP`. A safety property is a *regular safety property* if its bad prefixes can be described by a regular language (e.g., NFA/DFA).

The key for the NFA/DFA for the bad prefixes is that any final state should have a self-loop on any letter. Why? The idea is to define a set of minimum length bad sequences---meaning that there is no shorter prefix of a bad prefix such that itself is also a bad prefix. The meaning of these bad prefixes is that if any such finite sequence is observed that matches the bad prefix (even if the sequence continues), then it is an error. As such, the final states are syncs in the DFA/NFA. Once in a final state, always in a final state. The DFA/NFA for the bad prefixes is defined in a way that once you hit a final state, then report error even if the sequence continues. From here on out bad-prefixes are designated as the *never claim*.

Verifying regular safeties properties computes the synchronous product of the NFA for the model and the NFA for the never claim and checks if that product is empty using a depth-first-search.

  1. Define `AP` for the language 
  2. Create the NFA for the never claim
  3. Convert the model to an NFA
  4. Compute the synchronous product
  5. Perform a depth-first-search to see if the language is empty (if not empty, then the stack for the search is a counter-example)

The never-claim in SPIN is an NFA, and exiting the claim (reaching the closing brace) is the accepting state. The processes in SPIN are a transition system. Not an NFA. These must be converted to an NFA. This conversion must be done in a way consistent with SPIN since the goal is to really understand how SPIN works. SPIN creates the NFA in a way such that:

  * The transition system is total
  * The never-claim (property DFA) always steps first and then the transition system.

A total transition system means that every state always has a successor. In the case of SPIN, any state with no successor stutters (e.g., it repeats) as long as the never-claim is able to step. The `stutter.pml` file in an example of such behavior. For the lecture, any state without an outgoing transition in the transition system gets a self-loop.

SPIN always steps the never-claim first and then the system to form the state in the synchronous product. In lay-person terms, SPIN converts a transition system to an NFA in SPIN by moving the state labels to the outgoing edges. So for `s --> t` in the transition system, the corresponding NFA edge is `s -L(s)-> t` where `L(s)` is the letter. 

Work the example for the german traffic light controller on page 164. In the example `R/Y` is its own letter. See the paper notes in tap 5 for the conversion to only have `R`, `G`, and `Y` as letters.

## Converting a Transition System to an NFA Summarized

Define a transition system `TS` as a graph with the state labels in the nodes and actions on the edges. The actions are ignored in the verification. Only the state labels matter. Converting such a system to an NFA is easy. 

  1. Copy the structure of the `TS` giving each state a unique label 
  2. Add a self-loop to any state that does not have a successor
  2. Make every state accepting
  3. For all states, move the state's label to all of its outgoing edges

## Making sense of edge labels with Boolean Logic

Recall that the letters are members of the power set over `AP`. So a letter is actually a set `A \subseteq 2^AP`. That is confusing especially since the NFA is often written with Boolean expressions over `AP` on the edges. If the NFA has an edge with `yellow` on it, what it really means is any letter with `yellow` in it. In other words, the expression is true for any member of the power-set `2^AP` with the `yellow` proposition in it. Similarly, `!yellow` is any member of the power set that does not include yellow. 

**Warning:** this inconsistency can be very confusing, so be careful when working things by hand and when creating the never-claim.

## On-the-fly

Definition 4.16 on page 162 defines the "on-the-fly" algorithm for computing the synchronous product of a transition system and NFA to compute language intersection **but** it does not match what SPIN does because Definition 4.16 steps the transition system first rather than the never claim. The definition to match SPIN is below.

```
s --> t /\ q -L(s)-> p
----------------------
(s,q) -L(s)-> (t,p)
```

The modification effectively steps the property NFA first and then the transition system.

The algorithmic statement for *on-the-fly* as a depth-first-search:

  0. If the never-claim is not able to step given the current model state, then return
  1. For each enabled transition in the never claim given the current model state
      0. Update the claim state by following the transition
      1. If the never claim is at its final state, then report the error and exit
      2. For each enabled transition in the model
         0. Update the model state by following the transition
         1. Make the recursive call to the search with the current product state: never claim and model state
  
The state explosion comes from all the choices that have to be explored to resolve non-determinism. 

Rework the german traffic light *on-the-fly*.
Rework again only this time following the spin-code: german-traffic-light.pml

# Homework

`Psafe` is a safety property in positive form. It's complement describes the *bad prefixes* (e.g., the counter-examples).