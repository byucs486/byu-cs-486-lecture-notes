/* German Traffic Light Controller Page 164, Principles of model checking.h
 * 
 * Warning: AP = {Red,Yellow,Red-yellow, and Green} Be careful in the
 * translation. Figure 4.5 shows AP = {Red, Yellow}. If treat AP with mtype,
 * then it all works out with the set definition for the edge labels describing
 * the figure.
 * 
 * SPIN: 
 *  spin -DLTS -run german-traffic-light.pml 
 *  spin -DLTS -p -t german-traffic-light.pml (error trace) 
 */

#ifdef LTS
mtype = {red, yellow, red_yellow, green}
mtype light = yellow

active proctype gtl() {  
s0:
    light = red
s1:
	light = red_yellow
s2:
    if
    :: light = green
    :: light = red
    fi
s3:
    light = yellow
	goto s0
}

/* Reaching the end of the never claim indicates an accepting state */
never { 
q0:
  if
  :: light != red && light != yellow -> goto q0
  :: light != red && light == yellow -> goto q1
  :: light == red -> goto accept_q2
  fi
q1:
  if
  :: light != yellow -> goto q0
  :: light == yellow -> goto q1
  fi
accept_q2: goto accept_q2
  /* accept */
}

never {
false
}

#endif


#ifdef DO_LOOP
/* Replace with do-loop. 
 *
 * Warning: without "atomic", the light gets out of step with the never claim
 * and fails. The atomic makes it equivalent to the LTS version.
 */
mtype = {red, yellow, red_yellow, green} 
mtype light = yellow

active proctype gtl() {  
    do
    :: atomic {light == yellow      -> light = red}
	:: atomic {light == red         -> light = red_yellow}
    :: atomic {light == red_yellow  -> light = green}
    :: atomic {light == green       -> light = yellow}
    od
}

/* Reaching the end of the never claim indicates an accepting state */
never { 
q0:
  if
  :: light != red && light != yellow -> goto q0
  :: light != red && light == yellow -> goto q1
  :: light == red -> goto q2
  fi
q1:
  if
  :: light != yellow -> goto q0
  :: light == yellow -> goto q1
  fi
q2:
  /* accept */
}
#endif

#ifdef REMOTEREFS
/* Replace with do-loop. 
 *
 * Warning: without "atomic", the light gets out of step with the never claim
 * and fails. The atomic makes it equivalent to the LTS version.
 */
mtype = {red, yellow, red_yellow, green}

active proctype gtl() {  
    mtype light = yellow
    do
    :: atomic {light == yellow      -> light = red}
	:: atomic {light == red         -> light = red_yellow}
    :: atomic {light == red_yellow  -> light = green}
    :: atomic {light == green       -> light = yellow}
    od
}

/* Reaching the end of the never claim indicates an accepting state */
never { 
q0:
  if
  :: gtl:light != red && gtl:light != yellow -> goto q0
  :: gtl:light != red && gtl:light == yellow -> goto q1
  :: gtl:light == red -> goto q2
  fi
q1:
  if
  :: gtl:light != yellow -> goto q0
  :: gtl:light == yellow -> goto q1
  fi
q2:
  /* accept */
}
#endif

#ifdef BOOLEAN
/* Replace with do-loop. 
 *
 * Warning: without "atomic", the light gets out of step with the never claim
 * and fails. The atomic makes it equivalent to the LTS version.
 */

active proctype gtl() { 
    bool red = false, yellow = true
s0:
    atomic {
        red = true
        yellow = false
    }
s1:
    atomic {
        yellow = true
    }
s2: 
    atomic {
        red = false
        yellow = false
    } 
s3: 
    atomic {
        yellow = true
        goto s0
    }
}

/* 
 * Reaching the end of the never claim indicates an accepting state 
 *
 * AP = {red, yellow, green, red_yellow}
 * Encode with two bits red and yellow
 *
 * letter       | r_b   y_b
 * ----------------------------
 * red          | 1       0
 * yellow       | 0       1
 * green        | 0       0
 * red_yellow   | 1       1
 *
 * g0 (!red && !yellow): !(r_b && !y_b) && !(!r_b && y_b)
 *                          = (!r_b || y_b) && (r_b || !y_b
 *                          = !r_b && !y_b || y_b && r_b
 */
never { 
q0:
  if
  :: (!gtl:red && !gtl:yellow) || (gtl:red && gtl:yellow) -> goto q0
  :: !gtl:red && gtl:yellow  -> goto q1
  :: gtl:red && !gtl:yellow -> goto q2
  fi
q1:
  if
  :: !gtl:yellow -> goto q0
  :: gtl:yellow -> goto q1
  fi
q2:
  /* accept */
}
#endif
