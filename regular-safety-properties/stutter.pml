byte i = 1

active proctype a() {
  skip
  skip
  i = 0
}

never {
  if
  :: (i == 10)
  :: else
  fi
  do
  :: (i == 0) ->
    break
  :: else
  od
  true
  true
  true
  true
}