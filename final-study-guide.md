# Overview

The final covers liveness, safety, Buchi interections with transitions systems, temporal logic, BDDs, and CTL model checking. It is open book and open notes (including the lecture notes). It is targeted to 2-3 hours of student time.

# Study Resources

The [lecture notes](https://bitbucket.org/byucs486/byu-cs-486-lecture-notes/src/master/) and [homework 6 through 11](https://bitbucket.org/byucs486/byu-cs-486-homework/src/master/) are the primary resources for preparing for the exam.

# Comprehensive List of Topics

The below is a comprehensive list of topics on the exam.

  * Write safety and liveness properties (HW4)
  * Apply on-the-fly model checking to a given Buichi Automata and Kripke Structure to see if there is a common word in the two languages (HW5)
  * Add the least number of labels to a Kripke structure to satisfy a given temporal logic formula (HW6 and HW7).
  * Turn English sentences into LTL properties given a set of atomic propositions (HW6 and HW7).
  * Turn English sentences into CTL properties given a set of atomic propositions (HW6 and HW7).
  * Argue equivalent, or prove not equivalent, temporal logic expressions.
  * Play computer and compute the final BDD using the ITE algorithm for a small program that uses CUDD. Show the contents of the compute table and draw the final BDD (HW10).
  * Perform CTL model checking given a Kripke structure and CTL formula using least/greatest fix-points (HW9 and HW11). Requires that the formula be converted to only use EX, EG, and EU. Show each iteration on each fix-point.
