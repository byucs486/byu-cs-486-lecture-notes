# Objectives

  * Define omega regular languages
  * Write regular expressions for omega-languages given a Buchi Automaton
  * Compute the synchronous product of two Buchi Automaton
  * Determine in the language of a Buchi Automaton is empty (Double depth-first search)
  * Verify omega-regular properties on-the-fly (simplified and general intersection)
  * Verify liveness properties with Buchi Automaton
  * Verify liveness properties in SPIN with the never claim and accept labels

# Reading

* Sections 4.3 of [Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf)
* Chapter 9 of Model Checking available online at [http://lib.byu.edu](http://lib.byu.edu) (must be on campus) --- Chapter 7 in the newer version of the book

# Omega-regular Languages

Reasons over infinite words by introducing the omega (`w`-operator) which is like Kleene closure only for infinite repetition (the word never ends).

  * Union (`+`-operator) for NBAs `A1` and `A2` means `L_w(A1) \cup L_w(A2)`
  * Concatenation (`.`-operator) for NFA `A1` and NBA `A2` means `L(A1).L_w(A2)`

Write a few omega-regular expressions for liveness properties.

# Buichi Automaton

Reasons over infinite words rather than finite words. Omega (`w`) represents infinite repetition. The acceptance condition changes now. Assume that `p` is a run of the system given a infinite word.  Assume that `inf(p)` returns the set of states in the Buichi Automaton that occur infinitely often on the run `p`. The acceptance condition for Buichi Automaton (BA) is that some accepting state has to occur infinitely often in the run:

```
inf(p) \cap (F) \neq \emptyset
```

The intuition is that there must be some finite prefix that then enters a cycle that contains on accepting state.

Write the language for a few Buichi Automaton using both `*`, `+`, and `w` for infinite repetition.

Every omega regular expression has a corresponding NBA; however every NBA does not have an equivalent DBA.

Work a few problems that go from omega-regular expression to NBA and visa versa.

# Synchronous Product

The general form that computes the product of two Buichi Automaton (useful for fairness) must introduce an extra variable in the state to count accept states that have been seen. Intuitively, `A \product B` means that the variable denotes having seen an accept state in `A` with the value `1`. It denotes having seen an accept state in `A` and then later in `B` with the value `2`. The value `0` means in has not seen the accept state in `A` yet.

The product includes the same restrictions as in NFA synchronous product only the combined state is now `Q_A X Q_B X {0,1,2}` with the last part of the product being the counter. As before, both automaton must have transitions on the given letter. Different though are the accept states in the product. Those are defined as `Q_A X Q_B X {2}`. The rules for the counter `x` in a transitions are thus assuming `a -> a'` and `b -> b'` both an the letter `\alpha`. In other words creating the transition `(a,b,x) -> (a',b',y)` on `\alpha` iff both are able to transition on `\alpha` as mentioned previously and 

  * if `x = 0` and `a' \in F_A` then `y = 1`
  * if `x = 1` and `b' \in F_B` then `y = 2` (accept state)
  * if `x = 2` then `y = 0`
  * otherwise `y = x`

Notice that it strictly counts in order meaning it must see left first followed by right and then resets over again. It adds a constant overhead to the number of states.

Practice on the product of two automatons that alternate between `a` and `b`: one accepts infinite number of `a` letters and the other an infinite number of `b` letters with an any `a` or `b` between.

## Simplified algorithm

Every state in the transition system (i.e., model) is an accept state. As such, the intersection is simplified as the counter is not needed as any cycle in the model is accepting. If the property is in an accept state, then that state in an accept state in the product.

# Finding a Word in the Language (Double Depth-first Search)

The NFA check is a simple depth-first search to see if an accept state is reachable. BAs make that much harder as not only must it find the accept state, but it must find a cycle that contains the accept state and is reachable from the initial state. Tarjan's algorithm is able to do just that in linear time. 

The algorithm, deemed [double depth-first search](ddfs.ppt) nests a second depth-first search inside a primary depth-first search. The second search is triggered post-order (e.g. leaving the state having visited all the children) on accept states only. The second search looks for a state that it has seen before *and* is in the search stack of the first search. 

Work through the product automaton as an example.

## On-the-fly

Follow the same as in [regular safety properties](../regular-safety-properties/regular-safety-properties.md) only use the double depth-first search algorithm for the cycle detection.

## Verification in SPIN

The `accept` label denotes any state that is an accept state in the never claim. The never claim is able to *accept* in two ways now: exit the claim for regular safety properties or find an accepting cycle for omega regular properties.

Write never claims for a few models.