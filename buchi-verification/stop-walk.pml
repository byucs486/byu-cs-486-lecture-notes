mtype = {red, green}
mtype light = red

active proctype stopWalk() {
  do
  :: light == red ->
    light = green
  :: light == green ->
    light = red
  :: true ->
    skip
  od
}

ltl p0 {[] (<> (light == green)))}

never {
q0:
  if
  :: light == red ->
    goto accept_q1
  :: true ->
    goto q0
  fi
accept_q1:
  if
  :: light == red ->
    goto accept_q1
  :: else ->
    false
  fi
}