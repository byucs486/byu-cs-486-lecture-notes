# Objectives

  * Classify properties as either safety or liveness

# Reading

  * Section 3.3 and 3.4 in [Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf)

# Safety Versus Liveness

A *safety property* reasons about the history of execution and is characterized as having a finite counter-example. A finite counter-example is some history or execution that ends in a bad state. In other words, starting at the last state and looking back through the history of execution it is possible to determine that the property is violated. Colloquially, a safety property ensures that nothing bad happens.

A *liveness property reasons about the future of execution and is characterized as having an infinite counter-example. An infinite counter-example is some future execution that never includes a desired behavior. In other words, starting at the current state and looking forward into the future execution it is possible to determine that the property never holds. Colloquially, a liveness properties ensures that something good happens.

Characterize the following:

  * A use of a variable must be preceded by a definition -- Safety
  * When a file is opened it must subsequently be closed -- Liveness
  * You cannot shift from drive to reverse without passing through neutral  -- Safety
  * No pair of adjacent dining philosophers can be eating at the same time  -- Safety
  * The program will eventually terminate -- Liveness
  * The program is free of deadlock -- Safety

**Class Exercise:** find gospel principles, or scriptures, for safety and liveness properties. Identify the alphabet for each property and write it on the board in a mathematical form using English for any temporal properties.
