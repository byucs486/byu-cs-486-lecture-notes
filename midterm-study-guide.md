# Overview

Midterm one covers Promela, safety, liveness, Buchi Automaton, intersecting Buchi Automaton, checking for language emptiness in Buchi Automaton, and writing never claims in Promela. You are allowed one page of notes, 8.5 x 11, with no other restrictions. The target is 2 hours of student time to complete the exam.

# Study Resources

The [lecture notes](https://bitbucket.org/byucs486/byu-cs-486-lecture-notes/src/master/) and [homework 1 through 5](https://bitbucket.org/byucs486/byu-cs-486-homework/src/master/) are the primary resources for preparing for the exam. 

# Comprehensive List of Topics

The below is a comprehensive list of topics on the exam. If a topic is not on the below list, then that is not being tested in this midterm.

  * Translate an if-statement and while-loop to equivalent Promela
  * Write a shared memory model in Promela for a Consensus object
  * Write a message passing model in Promela for a Consensus object
  * Define atomic propositions for specifications and indicate for each property in the specification if the property is a safety property, liveness property, or both and describe the counter example(s) using the atomic propositions
  * Use omega-regular notation to describe the language detected by a given Buchi Automaton
  * Compute the synchronous product (i.e., the intersection) of two Buchi Automaton--this problem requires the general algorithm for computing the product as not every state in either of the automaton are accepting
  * Determine if the language detected by a given Buchi Automaton is empty using a double depth-first search
  * Write a never claim for a given safety property about when a combination lock opens
  * Write a never claim for a given liveness property about about the same combination lock

