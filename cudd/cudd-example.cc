#include <stdlib.h>
#include <iostream>
#include <assert.h>

#include "cuddObj.hh"

typedef Cudd bddMgr;

void 
test_BDD() {
	bddMgr mgr = bddMgr(0,0);

	BDD a = mgr.bddVar();
	BDD b = mgr.bddVar();
	BDD cin = mgr.bddVar();

	BDD fullAdder = ((a | b) & cin) | (a & b);

	std::vector<BDD> nodes;
	nodes.push_back(fullAdder);
	const char *inames[] = {"a", "b", "cin"};
	const char *onames[] = {"fullAdder(a,b,cin)"};
	FILE *fptr = fopen("fullAdder.dot", "w");
	mgr.DumpDot(nodes, (char **)inames, (char**)onames, fptr);
	fclose(fptr);
}

void
simple_graph() {
	bddMgr mgr = bddMgr(0,0);

	BDD s1 = mgr.bddVar();
	BDD s0 = mgr.bddVar();
	
	BDD s1p = mgr.bddVar();
	BDD s0p = mgr.bddVar();

	std::vector<BDD> ps;
	ps.push_back(s1);
	ps.push_back(s0);

	std::vector<BDD> ns;
	ns.push_back(s1p);
	ns.push_back(s0p);
	
	BDD delta = (~s1 & ~s0 & ~s1p & s0p) |
		          (~s1 & s0 & s1p & ~s0p)  |
							(s1 & ~s0 & s1p & s0p)   |
							(s1 & s0 & ~s1p & ~s0p);
	
	BDD S_0 = ~s1 & ~s0;
	BDD S_1 = S_0 | (S_0.SwapVariables(ps, ns)).AndAbstract(delta, s1p & s0p);
	BDD S_2 = S_1 | (S_1.SwapVariables(ps, ns)).AndAbstract(delta, s1p & s0p);
	BDD S_3 = S_2 | (S_2.SwapVariables(ps, ns)).AndAbstract(delta, s1p & s0p);
	BDD S_4 = S_3 | (S_3.SwapVariables(ps, ns)).AndAbstract(delta, s1p & s0p);
	
	assert(S_4 == S_3);

	std::vector<BDD> nodes;
	nodes.push_back(S_0);
	nodes.push_back(S_1);
	nodes.push_back(S_2);
	nodes.push_back(S_3);
	nodes.push_back(S_4);
	
	const char *inames[] = {"s1", "s0", "s1'", "s0'"};
	const char *onames[] = {"S_0", "S_1", "S_2", "S_3", "S_4"};
	FILE *fptr = fopen("S.dot", "w");
	mgr.DumpDot(nodes, (char **)inames, (char**)onames, fptr);
	fclose(fptr);
}

/*
Consider the following mutual exclusion algorithm that uses the shared
variable turn.

State: turn pc0 pc1

Process P1:
   while true do
0	   wait until (turn == 0)
1		 turn = 1
   od
 
Process P2:
   while true do
0		 wait until (turn == 1)
1		 turn = 0
   od
*/

// How do I compute a(v,...) == b(v',...) with BDD's with the variable ?
// What does equivalence mean in logic terms? 
BDD same(BDD a, BDD b) {
	// Bi-implication a iff b which means a -> b && b -> a
	return (~a | b) & (~b | a);
}

void
unnamedMutex() {
	bddMgr mgr = bddMgr(0,0);
	BDD pc0 = mgr.bddVar();
	BDD pc0p = mgr.bddVar();
	BDD pc1 = mgr.bddVar();
	BDD pc1p = mgr.bddVar();
	BDD turn = mgr.bddVar();
	BDD turnp = mgr.bddVar();

  std::vector<BDD> ps;
	ps.push_back(pc0);
	ps.push_back(pc1);
	ps.push_back(turn);

	std::vector<BDD> ns;
	ns.push_back(pc0p);
	ns.push_back(pc1p);
	ns.push_back(turnp);

	BDD R =
		// pc0 == 0 && turn == 0 && pc0p == 1 && turnp == turn
	  (~pc0 & pc0p & 
		 ~turn & ~turnp & 
		 same(pc1, pc1p)) |

    // pc0 == 0 && turn == 1 && pc0p == 0 && turnp == turn
		(~pc0 & ~pc0p & 
		 turn & turnp & 
		 same(pc1, pc1p)) |

		// pc0 == 1 && pc0p = 0 && turnp = true 
		(pc0 & ~pc0p & 
		 turnp & 
		 same(pc1, pc1p)) |

		// pc1 == 0 && turn == 1 && pc1p == 1 && turnp == turn
	  (~pc1 & pc1p & 
		 turn & turnp & 
		 same(pc0, pc0p)) |

    // pc1 == 0 && turn == 0 && pc1p == 0 && turnp == turn
		(~pc1 & ~pc1p & 
		 ~turn & ~turnp & 
		 same(pc0, pc0p)) |

		// pc1 == 1 && pc1p = 0 && turnp = false 
		(pc1 & ~pc1p & 
		 ~turnp & 
		 same(pc0, pc0p));
		 
	BDD S0 = (~pc0 & ~pc1 & ~turn);
	BDD S = S0;
	BDD cube = pc0 & pc1 & turn;
	BDD old;
	do {
		old = S;
		S = S | (S.AndAbstract(R, cube)).SwapVariables(ns, ps);
	} while(S != old);

	std::cout << "|S0| = " << S0.CountMinterm(ps.size()) << std::endl;
	std::cout << "|S| = " << S.CountMinterm(ps.size()) << std::endl;

	// AG !(pc0 /\ pc1)
	// = !EF(pc0 /\ pc1)
	// = \muZ.(pc0 /\ pc1) \/ EX Z
	BDD g = pc0 & pc1;
	BDD Z = g;
	cube = pc0p & pc1p & turnp;
	do {
		old = Z;
		Z = g | (Z.SwapVariables(ps, ns)).AndAbstract(R, cube);
	} while (Z != old);

	BDD not_EF_g = !Z;

	std::cout << "|!EF (pc0 /\\ pc1)| = " << not_EF_g.CountMinterm(ps.size()) << std::endl;
	std::cout << "|S0 /\\ not_EF_g| = " << (S0 & not_EF_g).CountMinterm(ps.size()) << std::endl;

	std::vector<BDD> nodes;
	nodes.push_back(S);
	nodes.push_back(not_EF_g);
	
	const char *inames[] = {"pc0", "pc0'", "pc1", "pc1'", "turn", "turn'"};
	const char *onames[] = {"S", "!EF (pc0 /\\ pc1)"};
	FILE *fptr = fopen("S.dot", "w");
	mgr.DumpDot(nodes, (char **)inames, (char**)onames, fptr);
	fclose(fptr);
}

int 
main(int argc, char* argv[]) {
	unnamedMutex();
  return 0;
}
