/*
Consider the following mutual exclusion algorithm that uses the shared
variable turn.

State: turn pc0 pc1

Process P1:
   while true do
0	   wait until (turn == 0)
1		 turn = 1
   od
 
Process P2:
   while true do
0		 wait until (turn == 1)
1		 turn = 0
   od
*/

byte turn = 0;

active proctype t0() {
loop:
  (turn == 0)
CS:
  turn = 1
  goto loop
}

active proctype t1() {
loop:
  (turn == 1)
CS:
  turn = 0
  goto loop
}

ltl p0 {[]!(t0@CS && t1@CS)}