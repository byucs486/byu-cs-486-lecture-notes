# Objectives

  * Create an expression tree from a CTL expression
  * Convert a CTL formula to an equivalent formula that only uses `EX`, `EG`, and `EU` and boolean connectives via De Morgan's law
  * Compute `EG` and `EU` via greatest and least fix-points
  * Verify CTL properties on simple models by hand using fix-points

# Reading

  * [ctl-sym-mc.ppt](ctl-sym-mc.ppt)
  * Page 31 of Model Checking available online at [http://lib.byu.edu](http://lib.byu.edu) (must be on campus) for De Morgan laws for CTL
  * Section 6.2.2 of Model Checking available online at [http://lib.byu.edu](http://lib.byu.edu) (must be on campus) for the fix-point formulations

# Expression Trees

Draw trees for several formulas. The key is that leaves should be atomic propositions. Inner nodes should be Boolean operations, `EX`, `EG`, or `EU`. Each operand is a child node. Order child nodes left to right so that `E f U g` has the `EU` node, left child `f`, and right child `g`.

The expressions tree orders the evaluation of the CTL formula for model checking. An post order traversal of the tree give the evaluation order: left to right traversal on children to get the values and then the node itself.

# De Morgan Laws for CTL

CTL model checking relies on efficient algorithms to compute `EX`, `EG`, and `EU`. The Boolean connectives are easy because those do not rely an a path per se. As such, converting formulas to only have these three operators is important. See Page 31 of Model Checking available online at [http://lib.byu.edu](http://lib.byu.edu) for the definitions.

  * `AX f` becomes `~EX(~f)`
  * `AF f` becomes `~EG(~f)`
  * `EF f` becomes `E[true U f]`
  * `AG f` becomes `~EF(~f)`
  * `A[f U g]` becomes `~E[~g U (~f /\ ~g)] /\ ~EG ~g`
  *  See book for `R` equivalences 

# CTL Model Checking

See [ctl-mc.ppt](ctl-mc.ppt).

# Computing EX f

Get the set of states for `f`. Add to that set any state that steps into a state in the set according to the transition relation.

# Computing E[f U g]

Defined by the fix-point calculation `\mu Z. g \/ (f /\ EX Z)`. The notation is a bit strange. The `\mu` means it is a least fix-point. `Z` is the set on which the fix-point is computed. As a least fix-point, the initial value of `Z` is the empty set. Everything after the `.` is the predicate transformer. It must be monotonic (increasing in the case of a least fix-point). Show examples on the microwave for computing the fix-point. Start with single path and work way up to the microwave. Also show examples from the homework.

# Computing EG f

Defined by the fix-point calculation `\nu Z.(f /\ EX Z)`. The `\nu` means it is a greatest fix-point. It this case, `Z` includes all states initially. As before, everything after `.` is the predicate transformer. In this case, it must monotonically decrease (which it does). Work examples on the board. Start with single path examples and then move to the microwave. Also show examples from the homework.

# Compare and Contrast to the non-fix-point formulation

Show the computation of `EU` via fix-point and non-fix-point on a single path model. How are they different? Repeat for `EG` --- SCC computation versus fix-point. How are they different?
