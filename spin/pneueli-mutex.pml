/*
The following program is a mutual exclusion protocol for two processes
due to Pnueli [118]. There is a single shared variable s which is
either 0 or 1, and initialiazed to 1. Besides, each process has a
shared Boolean variable y that initially equals 0. The program text
for process Pi (i=0,1) is as follows:

l0:loop forever do begin
l1: Noncritical section
l2: (y_i, s) := (1, i);
l3: wait until ((y_(1−i) = 0) || (s != i));
l4: Critical section
l5: y_i := 0
end.

Here, the statement (y_i, s) := (1, i); is a multiple assignment in
which variable y_i := 1 and s := i is a single, atomic step.

Create a verification model i PROMELA and use SPIN to verify
correctness (does not deadlock and ensures mutual exclusion)
*/

