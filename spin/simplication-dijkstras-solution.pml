/*
Consider the following mutual exclusion algorithm that was proposed
1966 [221] as a simplification of Dijkstra’s mutual exclusion algorithm in
case there are just two processes:

1: Boolean array b(0;1) integer k, i, j,
2: // comment This is the program for computer i, which may be
   // either 0 or 1, computer j != i is the other one, 1 or 0;
C0: b(i) := false;
C1: if k != i then begin
C2:    if not b(j) then
          go to C2;
       else
         k:=i;
         goto C1
       end;
    else
       critical section;
       b(i) := true;
       remainder of program;	
       go to C0;
    end

Here C0, C1, and C2 are program labels, and the word “computer” should be interpreted as process.

Create a verification model for the above algorithm and use SPIN to verify its correctness.

See page 59 for the n-process version of the algorithm at https://scholarscompass.vcu.edu/cgi/viewcontent.cgi?article=5524&context=etd
*/

byte b[2] = true
byte k = 0

active [2] proctype simple_dijkstras() {
  byte i = _pid
  byte j = 1 - i
C0:
  b[i] = false
C1:
  if
  :: k != i ->
C2:  if
     :: !b[j] -> goto C2
     :: else -> k = i
		goto C1
     fi
  :: else ->
CS:
     skip
     b[i] = true
     goto C0
  fi
}

never {
  do
  :: (simple_dijkstras[0]@CS && simple_dijkstras[1]@CS) -> break
  :: else -> skip
  od
}