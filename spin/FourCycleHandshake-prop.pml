bool req = 0;
bool ack = 0;

ltl ReqImpliesEventuallyAck {[](req -> <> ack)};
ltl fair {[](<>(req && ack)) -> [](req -> <> ack)}
ltl exists {!([](<>(req && ack)))}

active proctype sender() {
loc0:
  atomic {
	if
	:: (req == ack) ->
	   req = !req
	   goto loc1
	:: goto loc0
	fi;
  }

loc1:
  atomic{
	(ack == req)
	req = !req
	goto loc0
  }
}

active proctype receiver() {
loc0:
  atomic {
	if
	:: (req != ack) ->
	   ack = !ack
	   goto loc1
	:: goto loc0
	fi;
  }

loc1:
  atomic {
	(req != ack)
	ack = !ack
	goto loc0
  }
}

