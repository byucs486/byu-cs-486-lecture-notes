mtype = {apples, oranges, bananas}
mtype = {almonds, cashews, peacans}

chan fruit = [1] of {mtype, byte, byte}

proctype producer(chan output) {
   do
   :: true -> output!apples, 10, 10
   :: true -> output!apples(5, 2)
   :: true -> output!almonds(2, 2)
  od
}

proctype consumer(chan input) {
  mtype what = almonds;
  byte howMany;

  do
  :: input?what('s',_) ->
                      printf("%e(%d)\n", what, howMany)
  :: timeout -> break
  od
}

init {
  atomic {
    run producer(fruit);
    run consumer(fruit);
  }

  printf("%e\n", apples);
}

