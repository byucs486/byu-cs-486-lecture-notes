/*
   Consider the following leader election algorithm. P_1 .. P_N are in
   a ring topology with each process connected to its neighbor with a
   unidirectional channel moving clockwise. Each process has a unique
   identifier id. The aim is to elect the process with the highest
   identifier as the leader within the ring.

   send(id)
   while (true) do
      receive(m)
      if (m = id) then stop;
      if (m > id) then send(m);
   od

   Model the protocol as an N-channel system and verify if the correct process is elected.

*/


/* N must be greater than or equal to 2 */
#define N 3

chan channels[N] = [1] of {pid}

pid global_leader = 0;

proctype node(chan in, out) {
  pid leader
  out!_pid
end:
  do
  :: in?leader -> 
	 if
	 :: leader == _pid ->
		break
	 :: leader > _pid ->
		out!leader
	 :: else
	 fi
  od
  printf("leader = %d\n", leader)
  global_leader = leader 
}

init {
  byte index_in, index_out
  byte i
  atomic{
	for (i : 1 .. N) {
	  index_in = i - 1
	  index_out = i % N
	  run node(channels[index_in], channels[index_out])
	}
  }
endLO:
}

/* 1) There is at most one leader */
/* 2) A leader will always be elected */
/* 3) The elected leader has the highest ID number */
