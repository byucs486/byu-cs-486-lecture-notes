bool flag[2] = 0;
bool turn = 0;
byte nCrit = 0;

inline lock(i) {
  bool j = 1 - i
  flag[i] = 1
  turn = j
  (flag[j] == 0 || turn == i)
}

inline unlock(i) {
  flag[i] = 0
}

active [2] proctype peterson() {
again:
  lock(_pid)
critical:
  unlock(_pid)
  goto again
}

/* Mutual Exclusion */
ltl p0 {[]!(peterson[0]@critical && peterson[1]@critical)}

/* Any process that tries eventually locks */
ltl p1 {[](peterson[0]@again -> <>(peterson[0]@critical))}
ltl p2 {[](peterson[1]@again -> <>(peterson[1]@critical))}

/* Each process is able to request infinitely often */
ltl p3 {[](<>(peterson[0]@again))}
ltl p4 {[](<>(peterson[1]@again))}
