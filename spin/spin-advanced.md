# Objectives

  * Inline
  * Progress
  * End states
  * Channels
  * Message passing
  * `eval` and when to use it
  * `trace` to verify message properties
  * Exceptions (`unless`)
  * Enabled
  * `d_step` versus `atomic`

# Reading

The [spin manual page](http://spinroot.com/spin/Man/promela.html) is the very best source of information on spin.

# Inline

Inline is a macro and not a function call. There are no function calls in spin. The nearest thing to a function call is to run a process, and when wait, using a variable or channel, for that process to end. Return values have to passed through some global variable.

Rewrite a mutex class problem to use `inline` for the lock and unlock phases of the mutex.

# Progress

Ensures that every infinite execution has a infinite number of occurrences of any `progress` labeled statement. Use the `progress` label to verify fairness in one of the mutex examples. Note that it will require spin to use its notion of fairness in that it will not ignore a process forever. In other words, it will not consider schedules where any one process in ignored forever.

Split a mutex example so that each instance is in its own process. Run `spin -run -l <mutex>.pml`. It should report a non-progress cycle in one of the processes. Add in the progress label on the critical section to the processes reported in the counter example. Re-run. It should again report a non-progress cycle in the other process.

Weak fairness means a process will not be ignored in a cycle forever if it is always enabled. Forces spin to only look at infinite behavior that includes a transitions from the continuously enable process. Run `spin -run -l -f <mutex>.pml`. No error reported. Could have also just added the label, but that is a little different in meaning.

Not that fairness only works with progress or acceptance cycles.

# End

An invalid end state is an error where the system terminates but not all processes are at the end or a marked end state. Terminate in this case means there are no enabled transitions, so it is not a property of a reactive system. Only systems that terminate.

Modify a mutex to remove the loop to repeat the process and then add in an if-statement to non-deterministically block a process.

```
active proctype Pnueli0() {
    byte i = _pid
    byte j = 1 - _pid
loop:
    if
    :: true -> lock(i,j)
    :: true -> 
                false
    fi
cs:
    unlock(i)
    //goto loop
}
```

Run `spin -run -noclaim <mutex>.pml`. It should report the error. Look at the trace. Add in the end label and rerun. This label is sometimes more useful with message passing.

# Channels

Introduce `m-type` with its `%e` flag for `printf`. Create a producer-consumer example. Experiment with buffering. Also send multiple items through the channel such as a structure. Show how to match on literals. Introduce the syntax `white(token)` for readability. Be sure to make clear the non-copy and copy (`<val>`) form of the channel receive. Read carefully the send and receive man pages. Explore rendezvous channels too.

Create a system with two producers and one consumer. One producer produces items. The other tells the consumer what items to match (if any). The goal is to create an example for using the `eval` function.

```
mtype = {apples, oranges, bananas}
mtype = {almonds, cashews, peacans}

chan fruit = [3] of {mtype, byte, byte}

proctype producer(chan output) {
  do
  :: true -> output!apples, 10, 10
   :: true -> output!apples(5, 2)
  :: true -> output!almonds(2, 2)
  od
}

proctype consumer(chan input) {
  mtype what = almonds;
  byte howMany;

  do
  :: input??eval(what)(5,_) ->
                      printf("%e(%d)\n", what, howMany)
  :: timeout -> break
  od
}

init {
  atomic {
    run producer(fruit);
    run consumer(fruit);
  }

  printf("%e\n", apples);
}
```

Add examples of `empty(chan)`, `full(chan)`, `nfull(chan)`, `len(chan)`, etc.

Add in the `xr` and `xs` assertions to show how it forces read or write only access on a channel. Useful for pruning the state space in the verification.

Write a simple `trace` and `notrace` claim over events to demonstrate those actions. Note that copying something out of the queue is not considered an "event" so when used is not picked up in the `trace` or `notrace` claim. Make a receive a copy receive and see how it changes things (goes from error to no error). Also recall that invalid end states include non-empty message channels.

# Atomic

Page 16 of the SPIN book helps clarify `atomic` semantics. The key is that the first statement in the atomic block determines if it is executable. So in this case, the poll makes the executable decision, and then the whole block runs. Perfectly legal.

```
 if
    :: atomic { 
      fruit?[apples(_)] ->
      fruit?<what(howMany)>
    }
    :: atomic {
      fruit?[oranges(_)] ->
      fruit?what(howMany)
    }
  fi
```
