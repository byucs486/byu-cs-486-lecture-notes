# Reading

  * [http://spinroot.com/spin/whatispin.html](http://spinroot.com/spin/whatispin.html)
  * [http://spinroot.com/spin/Man/README.html](http://spinroot.com/spin/Man/README.html)

# VS Code

Install the VS Code extension *Promela* from Victor Duarte da Silva for text-editor highlighting of Promela files in VS Code. Be sure there is a version of the `spin` executable in the path. Run `spin` from the command line. The plugin is for formatting.

# OSX

Install from `homebrew` with `brew install spin` or download the appropriate [binary](https://github.com/nimble-code/Spin/tree/master/Bin).

# Linux

Download the appropriate [binary](https://github.com/nimble-code/Spin/tree/master/Bin).

# Windows

*Winzip* or *7-zip* is needed to unzip the binary. These are both free in the Windows Marketplace. Download the appropriate windows binary from the [binary](https://github.com/nimble-code/Spin/tree/master/Bin) distributions. Uncompress the binary with *winzip* or *7-zip*. Put it somewhere in the search path.

It is possible to run the Linux binary as well if desired. It's more complex but can be done. See below

  * Install WSL from this tutorial: https://docs.microsoft.com/en-us/windows/wsl/install-win10
  * Make sure to run the distro once to get it all set up. You can then close it (the distro doesn’t need to be open in a terminal to run in VS Code).
  * Install VS Code extension “Remote-WSL”: https://code.visualstudio.com/docs/remote/wsl
  * Install VS Code extension “Promela” from Victor Duarte da Silva for text-editor highlighting of Promela files in VS Code.
  * Download the Linux .gz file from Spinroot (http://spinroot.com/spin/whatispin.html). Use the terminal in VS code to copy the file from your Windows machine to your distro (you can access windows files from /mnt drive in distro).
  * Unarchive the .gz file and put it into /usr/bin. Add /usr/bin to your PATH variable in your .bashrc file.

Now you should be able to run `spin -?` from the terminal

# Quick Primer

Spin is one of the most stable and useable model checkers to date. It is relatively easy to install, and it has a graphical interface for those less accustomed to the command line. 

On the command line, when you go to test Spin, if you are getting command not found, then try to add . before the command to search the current directory as the current directly may not be in your search path: `./spin`

When you have Spin working, here are the most important commands:

  * `spin -n <some number> <promela file>`: random simulation of the promela file using `<some number>` as the random seed (makes the execution deterministic).
  * You may put in your Promela file `printf` commands. These will output when you run in simulation above as in the previous bullet. It is a great way to `printf`-debug your Promela mode.
  * `spin -run <file>.pml` or `spin -search <file>.pml` will model check your Promela model. If you have more than one ltl property specified, then `spin -run -ltl <property-name> <file>.pml` lets you name the property to verify.
  * If spin finds a error in model checking mode, then `spin -p -t <file>.pml` will display the error trace with line number.
  * Fairness constraints are added with an implication on your property: `ltl p0 {<fair-path-property> → <property-to-verify>}`.
  * Always check the left side of any implication to be sure it exists. For example, `ltl p1 {!<fair-path-property>}`, should report an error. That means a fair path exists in the model so the implication is not vacuously true.
  * Always try to modify your model to make your property pass and fail. That way you are absolutely ensured that your model is doing what you think it is doing.
  * If you want to use a specific program location in an ltl-property, then you can use `remoterefs`. See the SPIN manual page for details.
  * To map control locations to line numbers, you need to generate a pan program, and run pan -d (see below). Older more esoteric way to have spin do the model checking (I generally do not use this interface). 

## Old interface

  * `spin -a <file>.pml`: generates a pan.c file which is a model checker just for your Promela model `<file>.pml`
  * `gcc pan.c -o <file>` : builds the model checker executable for your Promela model.
  * `./<file> -a` : checks for accepting cycles
  * `./<file -a -N <name>`: indicates the property you want to check if multiple LTL properties are in the file
