# byu-cs-486-lecture-notes

1. Promela - [spin/spin.md](spin/spin.md) and [spin/spin-advanced.md](spin/spin-advanced.md)
2. Safety versus Liveness - [safety-liveness.md](safety-liveness.md)
3. Regular Safety Properties - [regular-safety-properties.md]
